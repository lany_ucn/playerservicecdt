package cloud.cave;

import cloud.cave.domain.Region;
import cloud.cave.server.common.PlayerRecord;
import cloud.cave.server.common.SubscriptionRecord;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestPlayerServiceCDT {

    private String address;
    private int port;

    private SubscriptionRecord sub1, sub2;
    private String id1 = "id02";
    private String id2 = "testplayer";
    private String positionString = "(0,1,0)";

    @Rule
    public GenericContainer playerServiceContainer = new GenericContainer("lanysom/playerservice").withExposedPorts(39021);

    @Before
    public void setUp()
    {
        //Get the address and port from the genric container
        address = playerServiceContainer.getContainerIpAddress();
        port = playerServiceContainer.getFirstMappedPort();

        sub1 = new SubscriptionRecord(id1, "Tutmosis", "grp01", Region.ODENSE);
        sub2 = new SubscriptionRecord(id2, "MrLongName", "grp02", Region.COPENHAGEN);

    }

    @Test
    public void shouldAddNewPlayer(){

        PlayerRecord pr = new PlayerRecord(sub1, positionString);

        Gson gson = new Gson();

        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://"+ address +":"+ port +"/msdo/v1/players/"+ id1))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(pr)))
                .build();

        try {
            HttpResponse<String> res = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString());

            assertThat(res.statusCode(), is(HttpServletResponse.SC_CREATED));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldUpdateExistingPlayer(){

        PlayerRecord pr = new PlayerRecord(sub2, positionString);

        Gson gson = new Gson();

        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://"+ address +":"+ port +"/msdo/v1/players/"+ id2))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(pr)))
                .build();

        try {
            HttpResponse<String> res = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString());

            assertThat(res.statusCode(), is(HttpServletResponse.SC_OK));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldNotUpdateOrCreatePlayerDueToMissingValues(){

        PlayerRecord pr = new PlayerRecord(sub2, positionString);

        pr.setPositionAsString(null);

        Gson gson = new Gson();

        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://"+ address +":"+ port +"/msdo/v1/players/"+ id2))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(pr)))
                .build();

        try {
            HttpResponse<String> res = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString());

            assertThat(res.statusCode(), is(HttpServletResponse.SC_BAD_REQUEST));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldGetPlayerByPlayerId(){
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://"+ address +":"+ port +"/msdo/v1/players/"+ id2))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        try {
            HttpResponse<String> res = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString());

            assertThat(res.statusCode(), is(HttpServletResponse.SC_OK));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldGetNotFoundOnNonExistingPlayerId(){
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://"+ address +":"+ port +"/msdo/v1/players/kasmanfaha"))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        try {
            HttpResponse<String> res = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString());

            assertThat(res.statusCode(), is(HttpServletResponse.SC_NOT_FOUND));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldGetListOfPlayersAtPosition(){
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://"+ address +":"+ port +"/msdo/v1/players/room/"+ positionString))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        try {
            HttpResponse<String> res = HttpClient.newHttpClient().send(req, HttpResponse.BodyHandlers.ofString());

            assertThat(res.statusCode(), is(HttpServletResponse.SC_OK));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
