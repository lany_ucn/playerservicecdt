# PlayerService

Dette er gruppe Golf's første udkast til PlayerService samt CDT. 

## CDT

Alle kald til servicen er lavet med HttpClient og der bruges testcontainere, så der er nødvendigt at referere 'org.testcontainers:testcontainers:1.16.0' i gradle filen.

TestPlayerServiceCDT.java filen kan placeres under integration pakken i cave projektet, så skulle den køre upåklageligt. 

Det bliver i øvrigt kun testet på status koder fra servicen, og ikke indholdet af svarene. Servicen virker kun med fake storage. 

Kildekoden til integrationstesten kan hentes på [BitBucket](https://bitbucket.org/lany_ucn/playerservicecdt/src/master/) og kan desuden clones med nedenstående kommando:
```
git clone https://lany_ucn@bitbucket.org/lany_ucn/playerservicecdt.git
```

## Docker Image

Docker image er tilgængeligt på docker hub: 
```
lanysom/playerservice:v1
```
Det kan startes med kommandoen:
```
docker run -d -p 39021:39021 lanysom/playerservice:v1
```
Det kan efterfølgende testes ved at skrive følgende url i en browser: http://localhost:39021/msdo/v1/players/health

Desuden kan det testes med f.eks. Postman eller curl.

Der er en hårdkodet player i fake storage med id: testplayer

```
GET localhost:39021/msdo/v1/players/testplayer

===============
Response: Status 200 OK
Body:
{
    "playerID": "testplayer",
    "playerName": "Test Player",
    "groupName": "Golf",
    "region": "AARHUS",
    "positionAsString": "(0,1,0)",
    "accessToken": "khjsdfkjghksdbgk"
}
```
Spørges der efter andre modtages status *404 Not Found*
```
GET localhost:39021/msdo/v1/players/testplayer2

===============
Response: Status 404 Not Found
Body: null
```
Det samme gælder hvis der spørges efter players i et rum. Hvis der ikke er nogen modtages status *404 Not Found* ellers *200 OK*
```
GET localhost:39021/msdo/v1/players/room/(0,1,0)

===============
Response status 200 OK
Body:
[
    {
        "playerID": "testplayer",
        "playerName": "Test Player",
        "groupName": "Golf",
        "region": "AARHUS",
        "positionAsString": "(0,1,0)",
        "accessToken": "khjsdfkjghksdbgk"
    }
]
```
```
GET localhost:39021/msdo/v1/players/room/(0,1,1)

===============
Response status 404 Not Found
Body:[]
```
Første gang en player POSTes til services oprettes data og der modtages en status *201 Created* 
```
POST localhost:39021/msdo/v1/players/testplayer8  
{
    "playerID": "testplayer8",
    "playerName": "Test Player I",
    "groupName": "Golf",
    "region": "AARHUS",
    "positionAsString": "(0,1,0)",
    "accessToken": "khjsdfkjghksdbgk"
}
===============
Response: Status 201 Created  
Location: /msdo/v1/players/testplayer8
Body: 
{
    "playerID": "testplayer8",
    "playerName": "Test Player I",
    "groupName": "Golf",
    "region": "AARHUS",
    "positionAsString": "(0,1,0)",
    "accessToken": "khjsdfkjghksdbgk"
}
```
Anden gang det samme request sendes, modtages en status *200 OK* fordi det er en opdatering af en eksisterende player.
```
POST localhost:39021/msdo/v1/players/testplayer8  
{
    "playerID": "testplayer8",
    "playerName": "Test Player II",
    "groupName": "Golf",
    "region": "AARHUS",
    "positionAsString": "(0,1,0)",
    "accessToken": "khjsdfkjghksdbgk"
}
===============
Response: Status 200 OK  
Location: /msdo/v1/players/testplayer8
Body: 
{
    "playerID": "testplayer8",
    "playerName": "Test Player II",
    "groupName": "Golf",
    "region": "AARHUS",
    "positionAsString": "(0,1,0)",
    "accessToken": "khjsdfkjghksdbgk"
}
```



